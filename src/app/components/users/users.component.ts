import { Component, OnInit } from '@angular/core';
import { User } from '../../classes/users';
import { UsersService } from '../../services/users.service'

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
public users: User[];

constructor(private user: UsersService) { }

ngOnInit(): void {
    this.user.getusers().then((data: User[]) => {
      this.users = data;
    });
  }

}
