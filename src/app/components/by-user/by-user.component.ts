import { Component, OnInit } from '@angular/core';
import { Album } from 'src/app/classes/album';
import { AlbumsService } from 'src/app/services/albums.service';

@Component({
  selector: 'app-by-user',
  templateUrl: './by-user.component.html',
  styleUrls: ['./by-user.component.scss']
})
export class ByUserComponent implements OnInit {
  public userId: number;
  public albums: Album[];

  constructor(private albumService: AlbumsService) { }

  ngOnInit(): void {
  }

  public getAlbum(): void{
    this.albumService.getAlbumByUser(Number(this.userId)).then(data => {
      this.albums = data;
    });
  }

}
