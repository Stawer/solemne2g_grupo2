import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByUserComponent } from './by-user.component';

describe('ByUserComponent', () => {
  let component: ByUserComponent;
  let fixture: ComponentFixture<ByUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
