import { Component, OnInit } from '@angular/core';
import { Album } from 'src/app/classes/album';
import { AlbumsService } from 'src/app/services/albums.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {
  public albums: Album[];

  constructor(private album: AlbumsService) { }
  
  ngOnInit(): void {
      this.album.getAlbums().then((data: Album[]) => {
        this.albums = data;
      });
    }

}
