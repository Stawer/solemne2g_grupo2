import { Component, OnInit } from '@angular/core';
import { Photo } from 'src/app/classes/photo';
import { PhotosService } from 'src/app/services/photos.service';
import { Album } from 'src/app/classes/album';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss']
})
export class AllComponent implements OnInit {
  public photos: Photo[];

  constructor(private photoService: PhotosService) {

  }

  public ngOnInit(): void {
    this.photoService.getAllPhotos().then((data: Photo[]) => {
      this.photos = data;
      console.log(this.photos);
    });
  }

  public deletePhoto(photoId: number) {
    this.photoService.deletePhoto(photoId).then(data => {
      let photoIndex = this.photos.map((x: Photo) => x.id).indexOf(photoId);
      this.photos.splice(photoIndex, 1)
    });
  }

  public createPhoto(albumId: number, title: string, url: string, thumbnailUrl: string): void {
    const photo = new Photo({
      albumId: albumId,
      title: title,
      url: url,
      thumbnailUrl: thumbnailUrl
    });
    this.photoService.createPhoto(photo).then(data => {
      data.id = this.photos.length;
      this.photos.push(data);
    });
  }
}
