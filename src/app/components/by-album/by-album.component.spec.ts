import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByAlbumComponent } from './by-album.component';

describe('ByAlbumComponent', () => {
  let component: ByAlbumComponent;
  let fixture: ComponentFixture<ByAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
