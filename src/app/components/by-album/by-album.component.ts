import { Component, OnInit } from '@angular/core';
import { Photo } from 'src/app/classes/photo';
import { PhotosService } from 'src/app/services/photos.service';

@Component({
  selector: 'app-by-album',
  templateUrl: './by-album.component.html',
  styleUrls: ['./by-album.component.scss']
})
export class ByAlbumComponent implements OnInit {
  public albumId: number;
  public photos: Photo[];

  constructor(private photoService: PhotosService) { }

  ngOnInit(): void {
  }

  public getPhoto(): void{
    this.photoService.getPhotoByAlbum(Number(this.albumId)).then(data => {
      this.photos = data;
    });
  }

  public deletePhoto(photoId: number) {
    this.photoService.deletePhoto(photoId).then(data => {
      let photoIndex = this.photos.map((x: Photo) => x.id).indexOf(photoId);
      this.photos.splice(photoIndex, 1)
    });
  }
}
