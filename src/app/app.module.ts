import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Rutas
import { AppRoutingModule } from './app-routing.module';

// Servicios
import { AlbumsService } from './services/albums.service';
import { PhotosService } from './services/photos.service';


//Componentes
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { ByUserComponent } from './components/by-user/by-user.component';
import { ByAlbumComponent } from './components/by-album/by-album.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { AlbumsComponent } from './components/albums/albums.component';


@NgModule({
  declarations: [
    AppComponent,
    AllComponent,
    ByUserComponent,
    ByAlbumComponent,
    NavbarComponent,
    HomeComponent,
    UsersComponent,
    AlbumsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AlbumsService,
    PhotosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
