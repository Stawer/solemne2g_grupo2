export class User {
    public id: number;
    public title: string;
    public name: string;
    public email: string;
  
    constructor(data: any = null) {
      if (data) {
        this.id = data.id;
        this.title = data.title;
        this.name = data.name;
        this.email = data.email;
      }
    }
  }
  