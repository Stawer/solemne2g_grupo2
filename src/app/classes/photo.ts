export class Photo {
    public albumId: number;
    public id: number;
    public title: string;
    public url: string;
    public thumbnailUrl: string;
  
    constructor(data: any = null) {
      if (data) {
        this.albumId = data.userId;
        this.id = data.id;
        this.title = data.title;
        this.url = data.url;
        this.thumbnailUrl = data.thumbnailUrl;
      }
    }
  }
  