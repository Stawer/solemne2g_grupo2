export class Album {
    public userId: number;
    public id: number;
    public title: string;
  
    constructor(data: any = null) {
      if (data) {
        this.userId = data.userId;
        this.id = data.id;
        this.title = data.title;
      }
    }
  }
  