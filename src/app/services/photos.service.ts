import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Photo } from '../classes/photo';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  public uri = 'https://jsonplaceholder.typicode.com/photos';
  public filterByAlbum = '?albumId=';
  public limit = '?_limit=50';

  constructor(private http: HttpClient) { }

  /*
  Listar todas las Photo
  Obtener Photos por Album
  Crear Photo
  Update Photo
  Eliminar Photo
  */
 
  public async getAllPhotos(): Promise<Photo[]> {
    return new Promise<Photo[]>((resolve, reject) => {
      this.http.get(`${this.uri}${this.limit}`).subscribe((data: any) => {
        resolve(data.map(value =>  new Photo(value)));
      });
    });
  }

  public async getPhotoByAlbum(albumId: number): Promise<Photo[]> {
    return new Promise<Photo[]>((resolve, reject) => {
      this.http.get(`${this.uri}${this.filterByAlbum}${albumId}`).subscribe((data: any) => {
        resolve(data.map(value => new Photo(value)));
      });
    });
  }

  public async createPhoto(photo: Photo): Promise<Photo> {
    return new Promise<Photo>((resolve, reject) => {
      this.http.post(this.uri, photo).subscribe((data: any) => {
        photo.id = data.id;
        resolve(photo);
      });
    });
  }

  public async updatePhoto(photoId: number, photo: Photo): Promise<Photo> {
    return new Promise<Photo>((resolve, reject) => {
      this.http.patch(`${this.uri}/${photoId}`, photo).subscribe((data: any) => {
        resolve(data);
      });
    });
  }

  public async deletePhoto(photoId: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http.delete(`${this.uri}/${photoId}`).subscribe((data: any) => {
        resolve();
      });
    });
  }
}
