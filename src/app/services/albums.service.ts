import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Album } from '../classes/album';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  public uri = 'https://jsonplaceholder.typicode.com/albums';
  public filterByUser = '?userId=';
  public limit = '?_limit=20';

  constructor(private http: HttpClient) { }

   /*
  Listar Album segun User
  */
 
  public async getAlbumByUser(userId: number): Promise<Album[]> {
    return new Promise<Album[]>((resolve, reject) => {
      this.http.get(`${this.uri}${this.filterByUser}${userId}`).subscribe((data: any) => {
        resolve(data.map(value => new Album(value)));
      });
    });
  }

  public async getAlbums(): Promise<Album[]> {
    return new Promise<Album[]>((resolve, reject) => {
        this.http.get(`${this.uri}${this.limit}`).subscribe((data: any) => {
            resolve(data.map(value => new Album(value)));
        });
    });
}

}
