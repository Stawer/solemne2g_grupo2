import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllComponent } from './components/all/all.component';
import { ByUserComponent } from './components/by-user/by-user.component';
import { ByAlbumComponent } from './components/by-album/by-album.component';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { AlbumsComponent } from './components/albums/albums.component';



const routes: Routes = [
  {path: 'home', component: HomeComponent },
  {path: 'all', component: AllComponent},
  {path: 'byalbum', component: ByAlbumComponent},
  {path: 'byuser', component: ByUserComponent},
  {path: 'users', component: UsersComponent},
  {path: 'albums', component: AlbumsComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
